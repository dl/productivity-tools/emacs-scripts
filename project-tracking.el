;;; project-tracking.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2023 Lukas C. Bossert / Jonathan Hartman
;;
;; Author: Lukas C. Bossert <bossert@itc.rwth-aachen.de>
;; Maintainer: Lukas C. Bossert <bossert@itc.rwth-aachen.de>
;; Created: October 30, 2023
;; Modified: October 30, 2023
;; Version: 0.0.1
;; Package-Requires: ((emacs "24.3"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Enable to track time in an prjoct
;;
;;; Code:

;; clock into project node
(defvar org-roam-clocking-heading nil
  "Variable to store the last selected heading for clock-in/clock-out.")

(defun org-roam--extract-headings (file)
  "Extract all headings from the specified Org-Roam FILE."
  (with-temp-buffer
    (insert-file-contents file)
    (goto-char (point-min))
    (org-element-map (org-element-parse-buffer) 'headline
      (lambda (headline)
        (org-element-property :title headline)))
    ))

(defun org-roam-clock-in (heading)
  "Clock in for the specified HEADING in the current buffer."
  (org-clock-in)
  (save-buffer)
  (setq org-roam-clocking-heading heading)
  (message "Clock-in completed for %s." heading))

(defun org-roam-clock-out (heading)
  "Clock out for the specified HEADING in the current buffer."
  (org-clock-out)
  (save-buffer)
  (setq org-roam-clocking-heading nil)
  (message "Clock-out completed (for %s.)" heading))

(defun project-tracking ()
  "Toggle clock-in/clock-out for a project node."
  (interactive)
  (let* ((node-id "4bdbb906-84d5-4642-815c-2db7cf03295f") ;; REFACTOR
         (file (org-roam-node-file (org-roam-node-from-id node-id))))
    (if (and file (file-exists-p file))
        (with-current-buffer (find-file-noselect file)
          (widen)
          (read-only-mode -1) ;; Ensure buffer is writable
          (org-mode)
          (goto-char (point-min))
          (if org-roam-clocking-heading
              (let ((heading org-roam-clocking-heading))
                (if (re-search-forward (format "^\\*+ %s" heading) nil t)
                    (org-roam-clock-out heading)
                  (message "Heading not found for clock-out.")))
            (let* ((headings (org-roam--extract-headings file))
                   (selected-heading (completing-read "Select heading: " headings)))
              (message "Selected heading: %s" selected-heading)
              (if (org-clock-is-active)
                  (org-roam-clock-out selected-heading)
                (if (re-search-forward (format "^\\*+ %s" selected-heading) nil t)
                    (org-roam-clock-in selected-heading)
                  (goto-char (point-max))
                  (insert (format "\n* %s\n" selected-heading))
                  (org-roam-clock-in selected-heading))))
            (org-clock-update-mode-line)
            (save-buffer))))))
;;; clock into project end

(defun project-tracking-testing ()
  (interactive)
  (message "Project Tracking Test works!")
)


(provide 'project-tracking)
;;; project-tracking.el ends here
