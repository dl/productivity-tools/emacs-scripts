;;; diary-custom-entries.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2023 Lukas C. Bossert
;;
;; Author: Lukas C. Bossert <bossert@itc.rwth-aachen.de>
;; Maintainer: Lukas C. Bossert <bossert@itc.rwth-aachen.de>
;; Created: November 29, 2023
;; Modified: November 29, 2023
;; Version: 0.0.1
;; Keywords: Symbol’s value as variable is void: finder-known-keywords
;; Homepage: https://github.com/lukascbossert/diary-entries
;; Package-Requires: ((emacs "24.3"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  This is meant for an easy way to track certain daily events.
;;
;;; Code:


(setq lcb/custom-entries-list '("SMA" "HYBRID" "SICK" "CHILDSICK"))

(defun lcb/diary-custom-entry (tag)
  "Log a custom entry with the specified TAG for the current date in the PROPERTIES of the Diary file."
  (interactive
   (list (completing-read "Select tag: " lcb/custom-entries-list nil t)))

  ;; Get the current date in the required format
  (let ((date (format-time-string "%Y-%m-%d")))
    ;; Navigate to the corresponding date entry in the org-roam-dailies file
    (org-roam-dailies-goto-date date)
    ;; Add the specified tag to the PROPERTIES with the value 'true'
    (org-entry-put nil tag "true")))

(defun lcb/extract-entries-by-tag (tag)
  "Extract entries with the specified TAG and their associated dates from org-roam-dailies-directory files."
  (let ((entries '()))
    ;; Iterate through all org-roam-dailies files
    (dolist (file (directory-files (expand-file-name org-roam-dailies-directory org-roam-directory) t "\\.org$"))
      (with-temp-buffer
        (insert-file-contents file)
        (goto-char (point-min))
        (message "Processing file: %s" file)
        ;; Get the title of the file (usually the date)
        (let ((title (save-excursion
                       (when (re-search-forward "^\\(#\\+title:\\|\\*\\)\\s-+\\(.*\\)" nil t)
                         (match-string-no-properties 2)))))
          ;; Search for entries with the specified tag in the file
          (while (re-search-forward (format "^:%s: *true" tag) nil t)
            (let ((date (and title (string-trim title))))
              ;; If a matching entry is found, add it to the entries list
              (when date
                (push (list tag date) entries))
              (message "Entry found: %s - %s" tag date))))))
    entries))

(defun lcb/count-entries-by-tag-and-year (tag)
  "Count entries with the specified TAG per year from org-roam-dailies-directory files."
  (let ((counts (make-hash-table :test 'equal))) ; Create a hash table to store counts, using equal comparison for keys
    (dolist (file (directory-files (expand-file-name org-roam-dailies-directory org-roam-directory) t "\\.org$"))
      (with-temp-buffer
        (insert-file-contents file)
        (goto-char (point-min))
        (message "Processing file: %s" file)
        (let ((title (save-excursion
                       (when (re-search-forward "^\\(#\\+title:\\|\\*\\)\\s-+\\(.*\\)" nil t)
                         (match-string-no-properties 2))))) ; Extract the title from the file

          (while (re-search-forward (format "^:%s: *true" tag) nil t)
            (let ((year (and title (string-match "\\([0-9]+\\)-[0-9]+-[0-9]+.*" title)
                             (match-string 1 title)))) ; Extract the year from the title
              (when year
                (puthash year (1+ (gethash year counts 0)) counts))))))) ; Update the count for the corresponding year

    counts)) ; Return the final counts hash table
             ;
             ;
(defun lcb/diary-list-of-custom-entries ()
  "List entries with specified tags, their corresponding dates, and counts per year."
  (interactive)
  (let ((all-entries '())
        (all-counts (make-hash-table :test 'equal))
        custom-entries)

    ;; Prompt for multiple categories
    (setq custom-entries (completing-read-multiple "Select tags: "
                                                   lcb/custom-entries-list nil t))

    ;; Validate selected tags
    (unless (cl-every (lambda (tag) (member tag lcb/custom-entries-list)) custom-entries)
      (error "Invalid tag selected"))

    ;; Prepare a buffer for displaying results
    (with-current-buffer (get-buffer-create "*Custom Log Entries and Counts*")
      ;; Set the buffer to org-mode
      (org-mode)
      ;; Clear the buffer
      (erase-buffer)

      ;; Display counts per year in a table
      (insert "\n#+name: Counts per year\n")
      (insert "|------+---|\n")
      (insert "| Year |")
      (dolist (tag custom-entries)
        (insert (format " %-7s |" tag)))
      (insert "\n")
      (insert "|------+")
      (dolist (tag custom-entries)
        (insert "--------|"))
      (insert "\n")
      ;; Iterate through each tag
      (dolist (tag custom-entries)
        ;; Extract entries for the current tag
        (setq all-entries (nconc all-entries (lcb/extract-entries-by-tag tag)))
        ;; Count entries by tag and year
        (let ((counts (lcb/count-entries-by-tag-and-year tag)))
          ;; Sum up counts for each year across all tags
          (maphash
           (lambda (year count)
             (let ((current-count (gethash year all-counts 0)))
               (puthash year (+ current-count count) all-counts)))
           counts)))
      (maphash
       (lambda (year total-count)
         (insert (format "| %4s |" year))
         (dolist (tag custom-entries)
           (let ((count (gethash year all-counts 0)))
             (insert (format " %6d |" count)))))
       all-counts)
      (insert "\n|------+")
      (dolist (tag custom-entries)
        (insert "--------|"))
      (insert "\n")

      ;; Display an overview table for each entry
      (insert "\n#+name: Overview per entry\n")
      (insert "|-------------------------------|\n")
      (insert "| Tag    | Date                 |\n")
      (insert "|-------------------------------|\n")
      (dolist (entry all-entries)
        (insert (format "| %-6s | %-20s |\n" (car entry) (cadr entry))))
      (insert "|-------------------------------|\n")

      ;; Align the org table and display the results
      (org-table-align)
      ;; Update all tables in the buffer
      (org-table-iterate-buffer-tables)
      (pop-to-buffer (current-buffer))
      (revert-buffer))))



(provide 'diary-custom-entries)
;;; diary-custom-entries.el ends here
