;;; rwth-colors.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2023 Lukas C. Bossert
;;
;; Author: Lukas C. Bossert <bossert@itc.rwth-aachen.de>
;; Maintainer: Lukas C. Bossert <bossert@itc.rwth-aachen.de>
;; Created: October 30, 2023
;; Modified: October 30, 2023
;; Version: 0.0.1
;; Package-Requires: ((emacs "24.3"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Data for colors of the RWTH Aachen University
;;
;;; Code:


(defface rwth-blau-100
  '((t :foreground "#00549F"
     :weight bold ))
  "Color setting of RWTH Aachen University; rwth-blau-100"
  :group 'rwth-colors)

(defface rwth-blau-75
  '((t :foreground "#407FB7"
     :weight bold ))
  "Color setting of RWTH Aachen University; rwth-blau-75"
  :group 'rwth-colors)

(defface rwth-blau-50
  '((t :foreground "#8EBAE5" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-blau-50"
  :group 'rwth-colors)

(defface rwth-blau-25
  '((t :foreground "#C7DDF2" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-blau-25"
  :group 'rwth-colors)

(defface rwth-blau-10
  '((t :foreground "#E8F1FA" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-blau-10"
  :group 'rwth-colors)

(defface rwth-schwarz-100
  '((t :foreground "#000000" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-schwarz-100"
  :group 'rwth-colors)

(defface rwth-schwarz-75
  '((t :foreground "#646567" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-schwarz-75"
  :group 'rwth-colors)

(defface rwth-schwarz-50
  '((t :foreground "#9C9E9F" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-schwarz-50"
  :group 'rwth-colors)

(defface rwth-schwarz-25
  '((t :foreground "#CFD1D2" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-schwarz-25"
  :group 'rwth-colors)

(defface rwth-schwarz-10
  '((t :foreground "#ECEDED" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-schwarz-10"
  :group 'rwth-colors)

(defface rwth-magenta-100
  '((t :foreground "#E30066" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-magenta-100"
  :group 'rwth-colors)

(defface rwth-magenta-75
  '((t :foreground "#E96088" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-magenta-75"
  :group 'rwth-colors)

(defface rwth-magenta-50
  '((t :foreground "#F19EB1" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-magenta-50"
  :group 'rwth-colors)

(defface rwth-magenta-25
  '((t :foreground "#F9D2DA" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-magenta-25"
  :group 'rwth-colors)
(defface rwth-magenta-10
  '((t :foreground "#FDEEF0" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-magenta-10"
  :group 'rwth-colors)

(defface rwth-gelb-100
  '((t :foreground "#FFED00" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-gelb-100"
  :group 'rwth-colors)
(defface rwth-gelb-75
  '((t :foreground "#FFF055" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-gelb-75"
  :group 'rwth-colors)
(defface rwth-gelb-50
  '((t :foreground "#FFF59B" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-gelb-50"
  :group 'rwth-colors)
(defface rwth-gelb-25
  '((t :foreground "#FFFAD1" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-gelb-25"
  :group 'rwth-colors)
(defface rwth-gelb-10
  '((t :foreground "#FFFDEE" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-gelb-10"
  :group 'rwth-colors)

(defface rwth-petrol-100
  '((t :foreground "#006165" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-petrol-100"
  :group 'rwth-colors)
(defface rwth-petrol-75
  '((t :foreground "#2D7F83" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-petrol-75"
  :group 'rwth-colors)
(defface rwth-petrol-50
  '((t :foreground "#7DA4A7" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-petrol-50"
  :group 'rwth-colors)
(defface rwth-petrol-25
  '((t :foreground "#BFD0D1" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-petrol-25"
  :group 'rwth-colors)
(defface rwth-petrol-10
  '((t :foreground "#E6ECEC" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-petrol-10"
  :group 'rwth-colors)

(defface rwth-tuerkis-100
  '((t :foreground "#0098A1" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-tuerkis-100"
  :group 'rwth-colors)
(defface rwth-tuerkis-75
  '((t :foreground "#00B1B7" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-tuerkis-75"
  :group 'rwth-colors)
(defface rwth-tuerkis-50
  '((t :foreground "#89CCCF" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-tuerkis-50"
  :group 'rwth-colors)
(defface rwth-tuerkis-25
  '((t :foreground "#CAE7E7" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-tuerkis-25"
  :group 'rwth-colors)
(defface rwth-tuerkis-10
  '((t :foreground "#EBF6F6" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-tuerkis-10"
  :group 'rwth-colors)

(defface rwth-gruen-100
  '((t :foreground "#57AB27" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-gruen-100"
  :group 'rwth-colors)
(defface rwth-gruen-75
  '((t :foreground "#8DC060" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-gruen-75"
  :group 'rwth-colors)
(defface rwth-gruen-50
  '((t :foreground "#B8D698" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-gruen-50"
  :group 'rwth-colors)
(defface rwth-gruen-25
  '((t :foreground "#DDEBCE" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-gruen-25"
  :group 'rwth-colors)
(defface rwth-gruen-10
  '((t :foreground "#F2F7EC" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-gruen-10"
  :group 'rwth-colors)

(defface rwth-maigruen-100
  '((t :foreground "#BDCD00" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-maigruen-100"
  :group 'rwth-colors)
(defface rwth-maigruen-75
  '((t :foreground "#D0D95C" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-maigruen-75"
  :group 'rwth-colors)
(defface rwth-maigruen-50
  '((t :foreground "#E0E69A" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-maigruen-50"
  :group 'rwth-colors)
(defface rwth-maigruen-25
  '((t :foreground "#F0F3D0" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-maigruen-25"
  :group 'rwth-colors)
(defface rwth-maigruen-10
  '((t :foreground "#F9FAED" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-maigruen-10"
  :group 'rwth-colors)

(defface rwth-orange-100
  '((t :foreground "#F6A800" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-orange-100"
  :group 'rwth-colors)
(defface rwth-orange-75
  '((t :foreground "#FABE50" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-orange-75"
  :group 'rwth-colors)
(defface rwth-orange-50
  '((t :foreground "#FDD48F" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-orange-50"
  :group 'rwth-colors)
(defface rwth-orange-25
  '((t :foreground "#FEEAC9" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-orange-25"
  :group 'rwth-colors)
(defface rwth-orange-10
  '((t :foreground "#FFF7EA" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-orange-10"
  :group 'rwth-colors)

(defface rwth-rot-100
  '((t :foreground "#CC071E" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-rot-100"
  :group 'rwth-colors)
(defface rwth-rot-75
  '((t :foreground "#D85C41" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-rot-75"
  :group 'rwth-colors)
(defface rwth-rot-50
  '((t :foreground "#E69679" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-rot-50"
  :group 'rwth-colors)
(defface rwth-rot-25
  '((t :foreground "#F3CDBB" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-rot-25"
  :group 'rwth-colors)
(defface rwth-rot-10
  '((t :foreground "#FAEBE3" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-rot-10"
  :group 'rwth-colors)

(defface rwth-bordeaux-100
  '((t :foreground "#A11035" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-bordeaux-100"
  :group 'rwth-colors)
(defface rwth-bordeaux-75
  '((t :foreground "#B65256" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-bordeaux-75"
  :group 'rwth-colors)
(defface rwth-bordeaux-50
  '((t :foreground "#CD8B87" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-bordeaux-50"
  :group 'rwth-colors)
(defface rwth-bordeaux-25
  '((t :foreground "#E5C5C0" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-bordeaux-25"
  :group 'rwth-colors)
(defface rwth-bordeaux-10
  '((t :foreground "#F5E8E5" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-bordeaux-10"
  :group 'rwth-colors)

(defface rwth-violett-100
  '((t :foreground "#612158" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-violett-100"
  :group 'rwth-colors)
(defface rwth-violett-75
  '((t :foreground "#834E75" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-violett-75"
  :group 'rwth-colors)
(defface rwth-violett-50
  '((t :foreground "#A8859E" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-violett-50"
  :group 'rwth-colors)
(defface rwth-violett-25
  '((t :foreground "#D2C0CD" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-violett-25"
  :group 'rwth-colors)
(defface rwth-violett-10
  '((t :foreground "#EDE5EA" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-violett-10"
  :group 'rwth-colors)

(defface rwth-lila-100
  '((t :foreground "#7A6FAC" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-lila-100"
  :group 'rwth-colors)
(defface rwth-lila-75
  '((t :foreground "#9B91C1" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-lila-75"
  :group 'rwth-colors)
(defface rwth-lila-50
  '((t :foreground "#BCB5D7" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-lila-50"
  :group 'rwth-colors)
(defface rwth-lila-25
  '((t :foreground "#DEDAEB" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-lila-25"
  :group 'rwth-colors)
(defface rwth-lila-10
  '((t :foreground "#F2F0F7" :weight bold ))
  "Color setting of RWTH Aachen University; rwth-lila-10"
  :group 'rwth-colors)

(provide 'rwth-colors)
;;; rwth-colors.el ends here
